head = """


██████╗ ██╗   ██╗███████╗ ██████╗██████╗ ███████╗██╗    ██╗
██╔══██╗╚██╗ ██╔╝██╔════╝██╔════╝██╔══██╗██╔════╝██║    ██║
██████╔╝ ╚████╔╝ ███████╗██║     ██████╔╝█████╗  ██║ █╗ ██║
██╔═══╝   ╚██╔╝  ╚════██║██║     ██╔══██╗██╔══╝  ██║███╗██║
██║        ██║   ███████║╚██████╗██║  ██║███████╗╚███╔███╔╝
╚═╝        ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝╚══════╝ ╚══╝╚══╝ 
                                                           

-> A beautiful script to calculate threads and filets profiles for machining and modeling.

!!! All values are given in metric units (cm) !!!

To use it with full features, you need to install PIL python library with :

        >> pip install pillow
        
If you want thread mesurements : 1
If you want filet mesurements : 2

"""

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import sys

class Main():
    
    
    def __init__(self):
        self.list_pitch_1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        self.list_pitch_2 = [18, 20, 30, 40, 50, 60]
        self.angle = 60
        self.profile_template = Image.open("assets/img/profile_template.png")
        self.draw = ImageDraw.Draw(self.profile_template)
        self.font = ImageFont.load_default()
        
    
    def menu(self):
        print(head)
        user_choice = int(input("Which type do you want ? "))
        return user_choice
        
        
    def threads(self):
        u_threads = int(input("\n\nWhich profile do you want ?\n\nI want M"))
        if u_threads in self.list_pitch_1 :
            pitch = 1
        elif u_threads in self.list_pitch_2 :
            pitch = 2
        H = pitch*(3**(1/2)/2)
        Summit = H/8
        Valley = H/4
        H_end = H-Summit-Valley
        self.draw.text((275, 15),"{} cm".format(pitch),(0,0,0),font=self.font)
        self.draw.text((20, 150),"{:.{prec}f} cm".format(H, prec=5),(0,0,0),font=self.font)
        self.draw.text((460, 70),"{:.{prec}f} cm".format(Summit, prec=5),(0,0,0),font=self.font)
        self.draw.text((350, 210),"{:.{prec}f} cm".format(Valley, prec=5),(0,0,0),font=self.font)
        self.draw.text((460, 150),"{:.{prec}f} cm".format(H_end, prec=5),(0,0,0),font=self.font)
        self.draw.text((162, 75),"{}°".format(self.angle),(0,0,0),font=self.font)
        self.profile_template.save('profile_completed.png')
        self.profile_template.show()


    def filet(self):
        u_filets = int(input("\n\nWhich profile do you want ?\n\nI want M"))
        if u_filets in self.list_pitch_1 :
            pitch = 1
        elif u_filets in self.list_pitch_2 :
            pitch = 2
        H = pitch*(3**(1/2)/2)
        Summit = H/8
        Valley = H/4
        H_end = H-Summit-Valley
        self.draw.text((275, 15),"{} cm".format(pitch),(0,0,0),font=self.font)
        self.draw.text((20, 150),"{:.{prec}f} cm".format(H, prec=5),(0,0,0),font=self.font)
        self.draw.text((460, 70),"{:.{prec}f} cm".format(Summit, prec=5),(0,0,0),font=self.font)
        self.draw.text((350, 210),"{:.{prec}f} cm".format(Valley, prec=5),(0,0,0),font=self.font)
        self.draw.text((460, 150),"{:.{prec}f} cm".format(H_end, prec=5),(0,0,0),font=self.font)
        self.draw.text((162, 75),"{}°".format(self.angle),(0,0,0),font=self.font)
        self.profile_template.save('profile_completed.png')
        self.profile_template.show()


if __name__ == "__main__":
    toto = Main()
    toto.menu()
    if toto.menu() == 1 :
        toto.threads()
    elif toto.menu() == 2 :
        toto.filet()
    else :
        print("Please retry with a correct value !!!")
        sys.exit()
